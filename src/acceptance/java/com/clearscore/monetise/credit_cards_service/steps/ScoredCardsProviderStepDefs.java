package com.clearscore.monetise.credit_cards_service.steps;

import com.clearscore.monetise.credit_cards_service.stubs.ScoredCardsStubs;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class ScoredCardsProviderStepDefs {

    @Given("^The HTTP response from Scored-Cards provider was (.*)$")
    public void theHttpResponseFromScoredCardsProviderWas(Integer httpStatusCode) {
        ScoredCardsStubs.stubScoredCardsWithHttpStatusCode(httpStatusCode);
    }

    @Given("^Scored-Cards provider returned unexpected content$")
    public void scoredCardsProviderReturnedBadContent() {
        ScoredCardsStubs.stubScoredCardsWithBadResponse();
    }

    @Then("^Scored-Cards provider will be called to retrieve credit cards$")
    public void scoredCardsProviderWillBeCalledToRetrieveCreditCards() {
        ScoredCardsStubs.verifyScoredCards();
    }
}
