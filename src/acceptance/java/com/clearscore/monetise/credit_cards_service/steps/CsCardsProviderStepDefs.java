package com.clearscore.monetise.credit_cards_service.steps;

import com.clearscore.monetise.credit_cards_service.stubs.CsCardsStubs;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class CsCardsProviderStepDefs {

    @Given("^The HTTP response from CS-Cards provider was (.*)$")
    public void theHttpResponseFromCsCardsProviderWas(Integer httpStatusCode) {
        CsCardsStubs.stubCsCardsWithHttpStatusCode(httpStatusCode);
    }

    @Given("^CS-Cards provider returned unexpected content$")
    public void csCardsProviderReturnedBadContent() {
        CsCardsStubs.stubCsCardsWithBadResponse();
    }

    @Then("^CS-Cards provider will be called to retrieve credit cards$")
    public void csCardsProviderWillBeCalledToRetrieveCreditCards() {
        CsCardsStubs.verifyCsCards();
    }
}
