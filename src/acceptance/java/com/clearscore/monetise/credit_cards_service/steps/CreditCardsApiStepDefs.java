package com.clearscore.monetise.credit_cards_service.steps;

import com.clearscore.monetise.credit_cards_service.Card;
import com.clearscore.monetise.credit_cards_service.PersonDetails;
import com.clearscore.monetise.credit_cards_service.SpringTest;
import com.clearscore.monetise.credit_cards_service.utils.ResponseHolder;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import io.cucumber.datatable.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static com.clearscore.monetise.credit_cards_service.stubs.CsCardsStubs.stubCsCards;
import static com.clearscore.monetise.credit_cards_service.stubs.ScoredCardsStubs.stubScoredCards;
import static java.util.Arrays.asList;
import static java.util.Collections.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@SpringTest
public class CreditCardsApiStepDefs {

    @Value("${credit.cards.path}")
    private String creditCardsPath;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ResponseHolder responseHolder;

    private TypeReference<List<Card>> cardsTypeReference = new TypeReference<List<Card>>() {
    };

    private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();


    @Given("^Providers responded correctly$")
    public void providersAreRespondingCorrecly() {
        stubCsCards();
        stubScoredCards();
    }

    @When("^A user requests credit cards$")
    public void aUserRequestsCreditCards() {
        PersonDetails personDetails =
            new PersonDetails("John", "Smith", "1990/01/23", 500, "FULL_TIME", 28000);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Lists.newArrayList(APPLICATION_JSON));
        HttpEntity<PersonDetails> httpEntity = new HttpEntity<>(personDetails, httpHeaders);

        ResponseEntity<String> response = restTemplate.exchange(creditCardsPath, POST, httpEntity, String.class);
        responseHolder.set(response);
    }

    @Then("^The HTTP response status will be (.*)$")
    public void theHttpResponseStatusWillBe(Integer expectedStatus) {
        Integer actualStatus = responseHolder.geStatusCodeValue();
        assertThat(actualStatus).isEqualTo(expectedStatus);
    }

    @Then("^A list of credit card offers will be returned to the user$")
    public void aListOfCreditCardOffersWillBeReturned() throws IOException {
        List<Card> cards = OBJECT_MAPPER.readValue(responseHolder.getBody(), cardsTypeReference);

        assertThat(cards)
            .extracting("provider", "name", "applyUrl", "apr", "features", "cardScore")
            .containsExactly(
                tuple("ScoredCards", "ScoredCard Builder", "http://www.example.com/apply", 19.4,
                      asList("Supports ApplePay", "Interest free purchases for 1 month"), 0.002),
                tuple("CSCards", "SuperSaver Card", "http://www.example.com/apply", 21.4, emptyList(), 0.013),
                tuple("CSCards", "SuperSpender Card", "http://www.example.com/apply", 19.2,
                      singletonList("Interest free purchases for 6 months"), 0.013));
    }

    @Then("^the HTTP response headers should include$")
    public void theHttpResponseHeadersShouldInclude(DataTable dataTable) {
        Map<String, String> expectedHeaders = dataTable.asMap(String.class, String.class);
        Map<String, String> actualHeaders = responseHolder.getHeaders().toSingleValueMap();

        assertThat(expectedHeaders.entrySet()).allMatch(p -> actualHeaders.get(p.getKey()).contains(p.getValue()));
    }
}
