package com.clearscore.monetise.credit_cards_service.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static java.util.Objects.requireNonNull;

public class FileReader {

  public static String readFile(String path) {

    ClassLoader classLoader = ClassLoader.getSystemClassLoader();

    File file = new File(requireNonNull(classLoader.getResource(path)).getFile());

    String content = "";

    try {
      content = new String(Files.readAllBytes(file.toPath()));
    } catch (IOException e) {
      e.printStackTrace();
    }

    return content;
  }
}
