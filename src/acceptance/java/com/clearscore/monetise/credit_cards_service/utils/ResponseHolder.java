package com.clearscore.monetise.credit_cards_service.utils;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class ResponseHolder {

    private ResponseEntity<String> response;

    public void set(ResponseEntity<String> response) {
        this.response = response;
    }

    public Integer geStatusCodeValue() {
        return response.getStatusCodeValue();
    }

    public String getBody() {
        return response.getBody();
    }

    public HttpHeaders getHeaders() {
        return response.getHeaders();
    }
}
