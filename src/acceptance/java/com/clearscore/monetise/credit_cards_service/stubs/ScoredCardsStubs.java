package com.clearscore.monetise.credit_cards_service.stubs;

import com.clearscore.monetise.credit_cards_service.utils.FileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToJson;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;

public class ScoredCardsStubs {

    private static Logger LOGGER = LoggerFactory.getLogger(ScoredCardsStubs.class);

    private static final String SCORED_CARDS_ENDPOINT = "/v2/creditcards";

    private static String requestBody = FileReader.readFile("mocks/scored-cards-request.json");

    public static void verifyScoredCards() {
        verify(postRequestedFor(urlEqualTo(SCORED_CARDS_ENDPOINT)).withRequestBody(equalToJson(requestBody)));
    }

    public static void stubScoredCards() {
        String responseBody = FileReader.readFile("mocks/scored-cards-response.json");

        stubFor(post(SCORED_CARDS_ENDPOINT)
                    .withRequestBody(equalToJson(requestBody))
                    .willReturn(aResponse().withBody(responseBody).withStatus(200)));

        LOGGER.debug("Testing ScoredCards with \n requestBody: \n {} \n\n and responseBody: {}", requestBody,
                     responseBody);
    }

    public static void stubScoredCardsWithBadResponse() {
        String responseBody = "{\"response\": \"Bad content\"}";

        stubFor(post(SCORED_CARDS_ENDPOINT)
                    .withRequestBody(equalToJson(requestBody))
                    .willReturn(aResponse().withBody(responseBody).withStatus(200)));
    }

    public static void stubScoredCardsWithHttpStatusCode(Integer httpStatusCode) {
        stubFor(post(SCORED_CARDS_ENDPOINT)
                    .withRequestBody(equalToJson(requestBody))
                    .willReturn(aResponse().withStatus(httpStatusCode)));
    }
}
