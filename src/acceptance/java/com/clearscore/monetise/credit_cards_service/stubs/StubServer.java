package com.clearscore.monetise.credit_cards_service.stubs;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

@Component
public class StubServer {

    private static Logger LOGGER = LoggerFactory.getLogger(StubServer.class);

    private WireMockServer wireMockServer;

    @PostConstruct
    private void startStubServer() {
        wireMockServer = new WireMockServer(wireMockConfig().port(5002));
        wireMockServer.start();
        WireMock.configureFor("localhost", 5002);

        LOGGER.info("Stub server started on {}:{}", "localhost", 5002);
    }

    @PreDestroy
    private void stopStubServer() {
        LOGGER.info("Stopping stub server");
        wireMockServer.stop();
    }
}