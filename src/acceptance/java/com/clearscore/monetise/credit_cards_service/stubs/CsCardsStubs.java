package com.clearscore.monetise.credit_cards_service.stubs;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToJson;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;

import com.clearscore.monetise.credit_cards_service.utils.FileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CsCardsStubs {

    private static Logger LOGGER = LoggerFactory.getLogger(CsCardsStubs.class);

    private static final String CS_CARDS_ENDPOINT = "/v1/cards";

    private static String requestBody = FileReader.readFile("mocks/cs-cards-request.json");

    public static void verifyCsCards() {
        verify(postRequestedFor(urlEqualTo(CS_CARDS_ENDPOINT)).withRequestBody(equalToJson(requestBody)));
    }

    public static void stubCsCards() {
        String responseBody = FileReader.readFile("mocks/cs-cards-response.json");

        stubFor(post(CS_CARDS_ENDPOINT)
                    .withRequestBody(equalToJson(requestBody))
                    .willReturn(aResponse().withBody(responseBody).withStatus(200)));

        LOGGER.debug("Testing CSCards with \n requestBody: \n {} \n\n and response body: \n {}", requestBody,
                     responseBody);
    }

    public static void stubCsCardsWithBadResponse() {
        String responseBody = "{\"response\": \"Bad content\"}";

        stubFor(post(CS_CARDS_ENDPOINT)
                    .withRequestBody(equalToJson(requestBody))
                    .willReturn(aResponse().withBody(responseBody).withStatus(200)));
    }

    public static void stubCsCardsWithHttpStatusCode(Integer httpStatusCode) {
        stubFor(post(CS_CARDS_ENDPOINT)
                    .withRequestBody(equalToJson(requestBody))
                    .willReturn(aResponse().withStatus(httpStatusCode)));
    }
}
