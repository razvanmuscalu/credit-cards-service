package com.clearscore.monetise.credit_cards_service;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    plugin = {"pretty", "html:build/cucumber", "json:build/cucumber.json"},
    features = "src/acceptance/resources/features",
    glue = "classpath:com.clearscore.monetise.credit_cards_service",
    tags = "not @Ignore")
public class RunCukesTest {

}