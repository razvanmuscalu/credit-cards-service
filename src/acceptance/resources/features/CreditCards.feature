Feature: Credit Cards Feature

    Scenario: Should return OK HTTP status
        Given Providers responded correctly

        When A user requests credit cards

        Then The HTTP response status will be 200


    Scenario: Should return the credit card offers from providers
        Given Providers responded correctly

        When A user requests credit cards

        Then A list of credit card offers will be returned to the user


    Scenario: Should return the application/json content header
        Given Providers responded correctly

        When A user requests credit cards

        Then the HTTP response headers should include
            | Content-Type | application/json |