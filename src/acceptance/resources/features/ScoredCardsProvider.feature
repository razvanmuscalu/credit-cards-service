Feature: Scored-Cards Provider Feature


    Scenario: Should call Scored-Cards provider to retrieve credit cards
        Given Providers responded correctly

        When A user requests credit cards

        Then Scored-Cards provider will be called to retrieve credit cards


    Scenario Outline: Should return relevant HTTP status when Scored-Cards provider has problems
        Given Providers responded correctly
        And The HTTP response from Scored-Cards provider was <scored_cards_http_status_code>

        When A user requests credit cards

        Then The HTTP response status will be <cards_service_http_status_code>

        Examples:
            | scored_cards_http_status_code | cards_service_http_status_code |
            | 500                           | 502                            |
            | 400                           | 500                            |
            | 404                           | 500                            |


    Scenario: Should return relevant HTTP status when Scored-Cards provider returns unexpected content
        Given Providers responded correctly
        And Scored-Cards provider returned unexpected content

        When A user requests credit cards

        Then The HTTP response status will be 500