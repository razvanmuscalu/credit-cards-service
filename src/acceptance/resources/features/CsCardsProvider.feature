Feature: CS-Cards Provider Feature


    Scenario: Should call CS-Cards provider to retrieve credit cards
        Given Providers responded correctly

        When A user requests credit cards

        Then CS-Cards provider will be called to retrieve credit cards


    Scenario Outline: Should return relevant HTTP status when CS-Cards provider has problems
        Given Providers responded correctly
        And The HTTP response from CS-Cards provider was <cs_cards_http_status_code>

        When A user requests credit cards

        Then The HTTP response status will be <cards_service_http_status_code>

        Examples:
            | cs_cards_http_status_code | cards_service_http_status_code |
            | 500                       | 502                            |
            | 400                       | 500                            |
            | 404                       | 500                            |


    Scenario: Should return relevant HTTP status when CS-Cards provider returns unexpected content
        Given Providers responded correctly
        And CS-Cards provider returned unexpected content

        When A user requests credit cards

        Then The HTTP response status will be 500