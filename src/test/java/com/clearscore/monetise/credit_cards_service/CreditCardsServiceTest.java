package com.clearscore.monetise.credit_cards_service;

import com.clearscore.monetise.credit_cards_service.cs_cards.CsCard;
import com.clearscore.monetise.credit_cards_service.cs_cards.CsCardsService;
import com.clearscore.monetise.credit_cards_service.scored_cards.ScoredCard;
import com.clearscore.monetise.credit_cards_service.scored_cards.ScoredCardsService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CreditCardsServiceTest {

    @Mock
    private CsCardsService csCardsService;

    @Mock
    private ScoredCardsService scoredCardsService;

    @InjectMocks
    private CreditCardsService creditCardsService;

    private final PersonDetails personDetails = new PersonDetails("", "", "", 0, "", 0);

    @Test
    @DisplayName("should transform cs-cards provider")
    void shouldTransformCsCardsIntoCreditCardOffersWithCorrectProvider() {
        when(csCardsService.getCsCards(any())).thenReturn(
            singletonList(new CsCard("card1", "http://apply1", 21.4, 6.3, emptyList())));

        List<Card> cards = creditCardsService.calculateCreditCardsFromProviders(personDetails);

        assertThat(cards).extracting("provider").containsExactly("CSCards");
    }

    @Test
    @DisplayName("should transform cs-cards name")
    void shouldTransformCsCardsIntoCreditCardOffersWithCorrectName() {
        when(csCardsService.getCsCards(any())).thenReturn(
          singletonList(new CsCard("card1", "http://apply1", 21.4, 6.3, emptyList())));

        List<Card> cards = creditCardsService.calculateCreditCardsFromProviders(personDetails);

        assertThat(cards).extracting("name").containsExactly("card1");
    }

    @Test
    @DisplayName("should transform cs-cards apply URL")
    void shouldTransformCsCardsIntoCreditCardOffersWithCorrectApplyUrl() {
        when(csCardsService.getCsCards(any())).thenReturn(
          singletonList(new CsCard("card1", "http://apply1", 21.4, 6.3, emptyList())));

        List<Card> cards = creditCardsService.calculateCreditCardsFromProviders(personDetails);

        assertThat(cards).extracting("applyUrl").containsExactly("http://apply1");
    }

    @Test
    @DisplayName("should transform cs-cards apr")
    void shouldTransformCsCardsIntoCreditCardOffersWithCorrectApr() {
        when(csCardsService.getCsCards(any())).thenReturn(
          singletonList(new CsCard("card1", "http://apply1", 21.4, 6.3, emptyList())));

        List<Card> cards = creditCardsService.calculateCreditCardsFromProviders(personDetails);

        assertThat(cards).extracting("apr").containsExactly(21.4);
    }

    @Test
    @DisplayName("should transform cs-cards features")
    void shouldTransformCsCardsIntoCreditCardOffersWithCorrectFeatures() {
        when(csCardsService.getCsCards(any())).thenReturn(
            asList(new CsCard("card1", "http://apply1", 21.4, 6.3, asList("feature1", "feature2")),
                   new CsCard("card2", "http://apply2", 19.2, 5.0, emptyList()),
                   new CsCard("card3", "http://apply3", 19.2, 5.0, null)));

        List<Card> cards = creditCardsService.calculateCreditCardsFromProviders(personDetails);

        assertThat(cards)
            .extracting("features")
            .containsExactly(asList("feature1", "feature2"), emptyList(), emptyList());
    }

    @Test
    @DisplayName("should transform cs-cards card score")
    void shouldTransformCsCardsIntoCreditCardOffersWithCorrectCardScore() {
        when(csCardsService.getCsCards(any())).thenReturn(
          singletonList(new CsCard("card1", "http://apply1", 21.4, 6.3, emptyList())));

        List<Card> cards = creditCardsService.calculateCreditCardsFromProviders(personDetails);

        assertThat(cards).extracting("cardScore").containsExactly(0.013);
    }

    @Test
    @DisplayName("should transform scored-cards provider")
    void shouldTransformScoredCardsIntoCreditCardOffersWithCorrectProvider() {
        when(scoredCardsService.getScoredCards(any())).thenReturn(
          singletonList(new ScoredCard("card1", "http://apply1", 19.4, 0.8, emptyList(), emptyList())));

        List<Card> cards = creditCardsService.calculateCreditCardsFromProviders(personDetails);

        assertThat(cards).extracting("provider").containsExactly("ScoredCards");
    }

    @Test
    @DisplayName("should transform scored-cards name")
    void shouldTransformScoredCardsIntoCreditCardOffersWithCorrectName() {
        when(scoredCardsService.getScoredCards(any())).thenReturn(
          singletonList(new ScoredCard("card1", "http://apply1", 19.4, 0.8, emptyList(), emptyList())));

        List<Card> cards = creditCardsService.calculateCreditCardsFromProviders(personDetails);

        assertThat(cards).extracting("name").containsExactly("card1");
    }

    @Test
    @DisplayName("should transform scored-cards apply URL")
    void shouldTransformScoredCardsIntoCreditCardOffersWithCorrectApplyUrl() {
        when(scoredCardsService.getScoredCards(any())).thenReturn(
          singletonList(new ScoredCard("card1", "http://apply1", 19.4, 0.8, emptyList(), emptyList())));

        List<Card> cards = creditCardsService.calculateCreditCardsFromProviders(personDetails);

        assertThat(cards).extracting("applyUrl").containsExactly("http://apply1");
    }

    @Test
    @DisplayName("should transform scored-cards apr")
    void shouldTransformScoredCardsIntoCreditCardOffersWithCorrectApr() {
        when(scoredCardsService.getScoredCards(any())).thenReturn(
          singletonList(new ScoredCard("card1", "http://apply1", 19.4, 0.8, emptyList(), emptyList())));

        List<Card> cards = creditCardsService.calculateCreditCardsFromProviders(personDetails);

        assertThat(cards).extracting("apr").containsExactly(19.4);
    }

    @Test
    @DisplayName("should transform scored-cards features")
    void shouldTransformScoredCardsIntoCreditCardOffersWithCorrectFeatures() {
        when(scoredCardsService.getScoredCards(any())).thenReturn(
            asList(new ScoredCard("card2", "http://apply1", 19.5, 0.9, singletonList("feature1"), singletonList("feature2")),
                   new ScoredCard("card2", "http://apply2", 19.4, 0.8, emptyList(), emptyList()),
                   new ScoredCard("card3", "http://apply3", 19.4, 0.8, null, emptyList()),
                   new ScoredCard("card4", "http://apply4", 19.4, 0.8, emptyList(), null),
                   new ScoredCard("card5", "http://apply5", 19.4, 0.8, null, null)));

        List<Card> cards = creditCardsService.calculateCreditCardsFromProviders(personDetails);

        assertThat(cards)
            .extracting("features")
            .containsExactly(asList("feature1", "feature2"), emptyList(), emptyList(), emptyList(), emptyList());
    }

    @Test
    @DisplayName("should transform scored-cards card score")
    void shouldTransformScoredCardsIntoCreditCardOffersWithCorrectCardScore() {
        when(scoredCardsService.getScoredCards(any())).thenReturn(
          singletonList(new ScoredCard("card1", "http://apply1", 19.4, 0.8, emptyList(), emptyList())));

        List<Card> cards = creditCardsService.calculateCreditCardsFromProviders(personDetails);

        assertThat(cards).extracting("cardScore").containsExactly(0.002);
    }

    @Test
    @DisplayName("should sort cards by score")
    void shouldSortCreditCardOffersByCardScore() {
        when(csCardsService.getCsCards(any())).thenReturn(
            asList(new CsCard("cs-card1", "http://apply", 10.0, 1.0, emptyList()),
                   new CsCard("cs-card2", "http://apply", 10.0, 3.0, emptyList()),
                   new CsCard("cs-card3", "http://apply", 10.0, 5.0, emptyList())));
        when(scoredCardsService.getScoredCards(any())).thenReturn(
            asList(new ScoredCard("scored-card1", "http://apply", 10.0, 2.0, emptyList(), emptyList()),
                   new ScoredCard("scored-card2", "http://apply", 10.0, 4.0, emptyList(), emptyList()),
                   new ScoredCard("scored-card3", "http://apply", 10.0, 6.0, emptyList(), emptyList())));
        List<Card> cards = creditCardsService.calculateCreditCardsFromProviders(personDetails);

        assertThat(cards)
            .extracting("name")
            .containsExactly("cs-card1", "scored-card1", "cs-card2", "scored-card2", "cs-card3", "scored-card3");
    }
}
