package com.clearscore.monetise.credit_cards_service.scored_cards;

import com.clearscore.monetise.credit_cards_service.PersonDetails;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class PersonDetailsForScoredCards {

    @JsonProperty("first-name")
    private final String firstName;

    @JsonProperty("last-name")
    private final String lastName;

    @JsonProperty("date-of-birth")
    private final String dateOfBirth;

    @JsonProperty("score")
    private final Integer creditScore;

    @JsonProperty("employment-status")
    private final String employmentStatus;

    private final Integer salary;

  public static PersonDetailsForScoredCards fromPersonDetails(PersonDetails personDetails) {
    return new PersonDetailsForScoredCards(
        personDetails.getFirstName(),
        personDetails.getLastName(),
        personDetails.getDateOfBirth(),
        personDetails.getCreditScore(),
        personDetails.getEmploymentStatus(),
        personDetails.getSalary());
  }
}