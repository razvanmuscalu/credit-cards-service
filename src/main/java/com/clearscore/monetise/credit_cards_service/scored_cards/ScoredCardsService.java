package com.clearscore.monetise.credit_cards_service.scored_cards;

import com.clearscore.monetise.credit_cards_service.exception.CreditCardsException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.springframework.http.HttpStatus.BAD_GATEWAY;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@Component
public class ScoredCardsService {

    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;
    private final String scoredCardsPath;
    private final TypeReference<List<ScoredCard>> scoredCardsTypeReference;

    public ScoredCardsService(RestTemplate restTemplate, ObjectMapper objectMapper, @Value("${scored.cards.path}") String scoredCardsPath) {
        this.restTemplate = restTemplate;
        this.objectMapper = objectMapper;
        this.scoredCardsPath = scoredCardsPath;
        this.scoredCardsTypeReference = new TypeReference<List<ScoredCard>>() {
        };
    }

    public List<ScoredCard> getScoredCards(PersonDetailsForScoredCards personDetails) {
        try {
            ResponseEntity<String> response = restTemplate.postForEntity(scoredCardsPath, personDetails, String.class);

            return objectMapper.readValue(response.getBody(), scoredCardsTypeReference);
        } catch (HttpClientErrorException ex) {
            throw new CreditCardsException("Failed to call Scored-Cards provider correctly", INTERNAL_SERVER_ERROR);
        } catch (HttpServerErrorException ex) {
            throw new CreditCardsException("Scored-Cards provider failed to fulfill the request", BAD_GATEWAY);
        } catch (Exception e) {
            throw new CreditCardsException("Something unrecoverable has happened", INTERNAL_SERVER_ERROR);
        }
    }
}
