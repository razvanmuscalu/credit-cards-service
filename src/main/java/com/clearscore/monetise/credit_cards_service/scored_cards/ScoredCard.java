package com.clearscore.monetise.credit_cards_service.scored_cards;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Optional;
import lombok.Value;

import static java.util.Optional.ofNullable;

@Value
public class ScoredCard {

    public static final String SCORED_CARD_PROVIDER_NAME = "ScoredCards";

    private final String card;

    @JsonProperty("apply-url")
    private final String applyUrl;

    @JsonProperty("annual-percentage-rate")
    private final Double annualPercentageRate;

    @JsonProperty("approval-rating")
    private final Double approvalRating;

    private final List<String> attributes;

    @JsonProperty("introductory-offers")
    private final List<String> introductoryOffers;

    public Optional<List<String>> getAttributes() {
        return ofNullable(attributes);
    }

    public Optional<List<String>> getIntroductoryOffers() {
        return ofNullable(introductoryOffers);
    }
}