package com.clearscore.monetise.credit_cards_service;

import static java.lang.Math.floor;

@FunctionalInterface
public interface CardScoreFunction {

    double apply();

    static CardScoreFunction cardScore(double eligibility, double apr) {
        return () -> {
          double score = eligibility * (Math.pow(1 / apr, 2));
          return floor(score * 1000) / 1000;
        };
    }
}