package com.clearscore.monetise.credit_cards_service;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import lombok.Value;

@Value
public class Card {

    private final String provider;
    private final String name;

    @JsonProperty("apply-url")
    private final String applyUrl;

    private final Double apr;
    private final List<String> features;

    @JsonProperty("card-score")
    private final Double cardScore;
}
