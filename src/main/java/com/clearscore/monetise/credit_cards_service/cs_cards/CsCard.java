package com.clearscore.monetise.credit_cards_service.cs_cards;

import static java.util.Optional.ofNullable;

import java.util.List;
import java.util.Optional;
import lombok.Value;

@Value
public class CsCard {

  public static final String CS_CARD_PROVIDER_NAME = "CSCards";

  private final String cardName;
  private final String url;
  private final Double apr;
  private final Double eligibility;
  private final List<String> features;

  public Optional<List<String>> getFeatures() {
    return ofNullable(features);
  }
}