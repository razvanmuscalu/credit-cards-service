package com.clearscore.monetise.credit_cards_service.cs_cards;

import com.clearscore.monetise.credit_cards_service.PersonDetails;
import lombok.Value;

@Value
public class PersonDetailsForCsCards {

    private final String fullName;
    private final String dateOfBirth;
    private final Integer creditScore;

  public static PersonDetailsForCsCards fromPersonDetails(PersonDetails personDetails) {
    return new PersonDetailsForCsCards(
        personDetails.getFirstName() + " " + personDetails.getLastName(),
        personDetails.getDateOfBirth(),
        personDetails.getCreditScore());
  }
}
