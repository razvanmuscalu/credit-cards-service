package com.clearscore.monetise.credit_cards_service.cs_cards;

import com.clearscore.monetise.credit_cards_service.exception.CreditCardsException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.springframework.http.HttpStatus.BAD_GATEWAY;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@Component
public class CsCardsService {

    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;
    private final String csCardsPath;
    private final TypeReference<List<CsCard>> csCardsTypeReference;

    public CsCardsService(RestTemplate restTemplate, ObjectMapper objectMapper, @Value("${cs.cards.path}") String csCardsPath) {
        this.restTemplate = restTemplate;
        this.objectMapper = objectMapper;
        this.csCardsPath = csCardsPath;
        this.csCardsTypeReference = new TypeReference<List<CsCard>>() {
        };
    }

    public List<CsCard> getCsCards(PersonDetailsForCsCards personDetails) {

        try {
            ResponseEntity<String> response = restTemplate.postForEntity(csCardsPath, personDetails, String.class);

            return objectMapper.readValue(response.getBody(), csCardsTypeReference);
        } catch (HttpClientErrorException ex) {
            throw new CreditCardsException("Failed to call CS-Cards provider correctly", INTERNAL_SERVER_ERROR);
        } catch (HttpServerErrorException ex) {
            throw new CreditCardsException("CS-Cards provider failed to fulfill the request", BAD_GATEWAY);
        } catch (Exception e) {
            throw new CreditCardsException("Something unrecoverable has happened", INTERNAL_SERVER_ERROR);
        }
    }
}
