package com.clearscore.monetise.credit_cards_service;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class PersonDetails {

    private final String firstName;
    private final String lastName;

    @JsonProperty("dob")
    private final String dateOfBirth;

    @JsonProperty("credit-score")
    private final Integer creditScore;

    @JsonProperty("employment-status")
    private final String employmentStatus;

    private final Integer salary;
}
