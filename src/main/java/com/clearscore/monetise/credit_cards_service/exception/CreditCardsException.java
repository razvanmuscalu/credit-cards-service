package com.clearscore.monetise.credit_cards_service.exception;

import org.springframework.http.HttpStatus;

public class CreditCardsException extends RuntimeException {

    private final HttpStatus status;

    public CreditCardsException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
