package com.clearscore.monetise.credit_cards_service.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.http.ResponseEntity.status;

@ControllerAdvice(basePackages = "com.clearscore.monetise.credit_cards_service")
public class ExceptionControllerAdvice {

    @ExceptionHandler(CreditCardsException.class)
    public ResponseEntity handleCreditCardsException(CreditCardsException ex) {
        return status(ex.getStatus()).build();
    }
}
