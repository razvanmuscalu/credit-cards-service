package com.clearscore.monetise.credit_cards_service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.ok;

@RestController
public class CreditCardsController {

    private final CreditCardsService creditCardsService;

    @Autowired
    public CreditCardsController(CreditCardsService creditCardsService) {
        this.creditCardsService = creditCardsService;
    }

    @PostMapping(value = "/creditcards", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Card>> postCreditCards(@RequestBody PersonDetails personDetails) {

        List<Card> cards = creditCardsService.calculateCreditCardsFromProviders(personDetails);

        return ok().body(cards);
    }
}
