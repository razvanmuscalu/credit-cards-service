package com.clearscore.monetise.credit_cards_service;

import static com.clearscore.monetise.credit_cards_service.CardScoreFunction.cardScore;
import static java.util.Collections.emptyList;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.concat;

import com.clearscore.monetise.credit_cards_service.cs_cards.CsCard;
import com.clearscore.monetise.credit_cards_service.cs_cards.CsCardsService;
import com.clearscore.monetise.credit_cards_service.scored_cards.PersonDetailsForScoredCards;
import com.clearscore.monetise.credit_cards_service.scored_cards.ScoredCard;
import com.clearscore.monetise.credit_cards_service.scored_cards.ScoredCardsService;
import com.clearscore.monetise.credit_cards_service.cs_cards.PersonDetailsForCsCards;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class CreditCardsService {

    private final CsCardsService csCardsService;
    private final ScoredCardsService scoredCardsService;

    public CreditCardsService(CsCardsService csCardsService, ScoredCardsService scoredCardsService) {
        this.csCardsService = csCardsService;
        this.scoredCardsService = scoredCardsService;
    }

    public List<Card> calculateCreditCardsFromProviders(PersonDetails personDetails) {
        List<Card> csCards = getCsCards(personDetails);
        List<Card> scoredCards = getScoredCards(personDetails);

        return concat(csCards.stream(), scoredCards.stream()).sorted(comparing(Card::getCardScore)).collect(toList());
    }

    private List<Card> getCsCards(PersonDetails personDetails) {
        PersonDetailsForCsCards personDetailsForCsCards =
            PersonDetailsForCsCards.fromPersonDetails(personDetails);

        return csCardsService.getCsCards(personDetailsForCsCards).stream().map(c -> {
            double cardScore = cardScore(c.getEligibility(), c.getApr()).apply();

            return new Card(CsCard.CS_CARD_PROVIDER_NAME, c.getCardName(), c.getUrl(), c.getApr(),
                            c.getFeatures().orElse(emptyList()), cardScore);
        }).collect(toList());
    }

    private List<Card> getScoredCards(PersonDetails personDetails) {
        PersonDetailsForScoredCards personDetailsForScoredCards =
            PersonDetailsForScoredCards.fromPersonDetails(personDetails);

        return scoredCardsService.getScoredCards(personDetailsForScoredCards).stream().map(c -> {
            List<String> features = concat(c.getAttributes().orElse(emptyList()).stream(),
                                           c.getIntroductoryOffers().orElse(emptyList()).stream()).collect(toList());
            double cardScore = cardScore(c.getApprovalRating(), c.getAnnualPercentageRate()).apply();

            return new Card(ScoredCard.SCORED_CARD_PROVIDER_NAME, c.getCard(), c.getApplyUrl(), c.getAnnualPercentageRate(),
                            features, cardScore);
        }).collect(toList());
    }
}
