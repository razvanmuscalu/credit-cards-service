# credit-cards-service

A simple Spring Boot service that collates credit card offers from suppliers and returns them sorted to callers.

## How To Run

`mvn install` - to run all tests and build local jar in target directory

After Maven completed successfully, you can run the `start.sh` script providing the required environment variables.

There are also a couple of IntelliJ runners committed:
 - RunCukes
 - RunTests
 - Application
 
## Design

The service is built using Spring Boot and it runs on Tomcat.

Other servers available out-of-the-bos for Spring Boot are:
 - Undertow
 - Jetty
 
## Testing

The project has been developed in a TDD / BDD combined manner. 

There are Cucumber features which test the system end-to-end:
 - Given providers reply in a way
 - When a request is made to the service
 - Then a response is expected
 
Some of the main benefits with these tests are:
 - are easy to read from business point of view
 - should map directly to business requirements
 - test the Spring features
 - test the service as a black box
   - test the contracts (JSON request/response) between the caller and the service and between the service and the providers
   - should be re-usable even if the internal technology stack changes

There are also unit tests for the CreditCardsService class. 

The main reasons behind unit testing this class specifically are:
 - this class contains the core logic of the service
 - it allows for more granular testing of more detailed aspects
 - it prevents from writing too many Cucumber tests
 
## Deployment

The packaging of this service is a single JAR (which can be made to accept an external properties file).

Two possible solutions
 - deploy the JAR and the properties file directly in a VM
 - deploy the JAR and the properties file in a Docker container
   - Spring config server helps with the managing of properties especially with Docker containers
   
## Further Improvements   
   
I spent approximately 8 hours on building the service in the current shape.   

If I would have more time, I would probably:
 - write performance tests (e.g. Gatling) and provide some performance reports
 - use circuit breakers (e.g. Netflix Hystrix from Spring Cloud) for communicating with the providers
   - specify provider timeout thresholds and act accordingly (return 504 GATEWAY_TIMEOUT) 
 - improve the Cucumber tests (at the moment the stub requests and responses are hardcoded; it would allow for more granular testing if they could be specified in the feature files as tables)
 - package the service in a Docker container
 - build and use Spring config server

## Note

I have applied the provided card score formula with the provided eligibility and apr numbers and arrived at different results

| credit_card        | expected (as per guidelines) | actual |
| ------------------ | ---------------------------- | ------ |
| ScoredCard Builder | 0.212				        | 0.002  | 
| SuperSaver Card    | 0.137						| 0.013  |
| SuperSpender Card  | 0.135						| 0.013  |

I am not sure what is happening.