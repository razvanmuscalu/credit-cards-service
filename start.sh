echo "Starting Credit Cards Service"

java -Dserver.port=$HTTP_PORT -Dcs.cards.path=$CSCARDS_ENDPOINT -Dscored.cards.path=$SCOREDCARDS_ENDPOINT -jar target/credit-cards-service.jar &

echo $! > pid